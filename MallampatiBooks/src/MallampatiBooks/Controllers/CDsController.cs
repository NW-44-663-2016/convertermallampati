using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using MallampatiBooks.Models;

namespace MallampatiBooks.Controllers
{
    public class CDsController : Controller
    {
        private ApplicationDbContext _context;

        public CDsController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: CDs
        public IActionResult Index()
        {
            return View(_context.CD.ToList());
        }

        // GET: CDs/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            CD cD = _context.CD.Single(m => m.CDID == id);
            if (cD == null)
            {
                return HttpNotFound();
            }

            return View(cD);
        }

        // GET: CDs/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: CDs/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(CD cD)
        {
            if (ModelState.IsValid)
            {
                _context.CD.Add(cD);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(cD);
        }

        // GET: CDs/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            CD cD = _context.CD.Single(m => m.CDID == id);
            if (cD == null)
            {
                return HttpNotFound();
            }
            return View(cD);
        }

        // POST: CDs/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(CD cD)
        {
            if (ModelState.IsValid)
            {
                _context.Update(cD);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(cD);
        }

        // GET: CDs/Delete/5
        [ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            CD cD = _context.CD.Single(m => m.CDID == id);
            if (cD == null)
            {
                return HttpNotFound();
            }

            return View(cD);
        }

        // POST: CDs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            CD cD = _context.CD.Single(m => m.CDID == id);
            _context.CD.Remove(cD);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
