﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;

namespace MallampatiBooks.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Mallampati Books contact page.";

            return View();
        }

        public IActionResult Books()
        {
            ViewData["Message"] = "Mallampati  Books page.";

            return View();
        }

        public IActionResult Authors()
        {
            ViewData["Message"] = "Mallampati  Authors page.";

            return View();
        }

        public IActionResult CDs()
        {
            ViewData["Message"] = "Mallampati CDs page.";

            return View();
        }



        public IActionResult Error()
        {
            return View();
        }
    }
}
