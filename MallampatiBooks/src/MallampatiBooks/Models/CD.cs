﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace MallampatiBooks.Models
{
    public class CD
    {
        [ScaffoldColumn(false)]
        public int CDID { get; set; }
        [Required]
        [Display(Name = "CD NameName")]
        public string Name { get; set; }

        [Display(Name = "CD Singer")]
        public string Singer { get; set; }

        [Display(Name = "Price")]
        public int Price { get; set; }

        public virtual ICollection<CD> CDs { get; set; }
    }
}
